from configs.config import sentry
#from controllers.hash_image import GetImageHash
from controllers.producer import Producer
from fastapi import FastAPI, File, UploadFile, Form
from fastapi.middleware.cors import CORSMiddleware
from PIL import Image
from starlette.responses import StreamingResponse
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from typing import List


from decouple import config
import pika


app = FastAPI()

origins = ['*']

app.add_middleware(CORSMiddleware,allow_origins=origins)

@app.get('/')
async def home():
    return {"message": "Hello World API Image!"}


@app.post('/images')
async def images(files: List[UploadFile] = File(...)):
    hashes = []
    for file in files:
        img = await file.read()
        producer = Producer(img)
        hash = producer.call()
        img_hash = {
            "Name": file.filename,
            "Hash": hash
        }
        hashes.append(img_hash)
    return hashes


@app.get('monitors')
async def monitor():
    try:
        app.add_middleware(SentryAsgiMiddleware)
    except Exception:
        # pass silently if the Sentry integration failed
        raise Exception('Test sentry integration')
