from decouple import config
import pika
import uuid
import PIL.Image as Image

class Producer:

    def __init__(self, img):
        self.img = img
        BROKER = 'broker1'
        BROKER_PORT = config('BROKER_PORT', cast=int)
        USER = config('USER')
        PASSWORD = config('PASSWORD')
        VHOST = config('VHOST')
        
        uri = pika.URLParameters(f'amqp://{USER}:{PASSWORD}@{BROKER}:{BROKER_PORT}{VHOST}')

        self.connection = pika.BlockingConnection(uri)
        self.channel = self.connection.channel()

        self.queue_declared = self.channel.queue_declare('', exclusive=True)
        self.callback_queue = self.queue_declared.method.queue

        self.channel.basic_consume(
            queue = self.callback_queue,
            on_message_callback = self.on_response, 
            auto_ack = True
        )
        
    def on_response(self, ch, method, props, body):
        if self.id == props.correlation_id:
            body.decode("utf-8", "ignore")
            self.response = body

    def call(self):
        self.response = None
        self.id = str(uuid.uuid4())
        self.channel.basic_publish(exchange='', 
                routing_key='rpc_queue', 
                properties=pika.BasicProperties(
                    reply_to=self.callback_queue,
                    correlation_id = self.id,
                    delivery_mode=2
                ),
                body = self.img
            )
        while self.response is None:
            self.connection.process_data_events()
        return self.response
