import sentry_sdk
from decouple import config
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware


def sentry():
    return sentry_sdk.init(
        config('SENTRY-SDK'),
        environment=config('SENTRY_ENV'),
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0
    )
