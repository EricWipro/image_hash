import pika
from decouple import config
from libs.get_image_hash import GetImageHash
import base64
import PIL.Image as Image
import io

reconnect_on_failure = True

BROKER = config('BROKER_URL')
BROKER_PORT = config('BROKER_PORT')
USER = config('USER')
PASSWORD = config('PASSWORD')
VHOST = config('VHOST')

credentials = pika.PlainCredentials(USER, PASSWORD)
parameters = pika.ConnectionParameters(BROKER,
       BROKER_PORT,
       VHOST,
       credentials,
    )


def callback(ch, method, props, body):
    img = Image.open(io.BytesIO(body))
    hasher = GetImageHash(img)
    hash = hasher.generate_hash()

    ch.basic_publish(exchange='', 
                    routing_key=props.reply_to, 
                    properties=pika.BasicProperties(
                        correlation_id=props.correlation_id),
                    body=str(hash))
    ch.basic_ack(delivery_tag = method.delivery_tag)


connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='rpc_queue', durable=True)
retorno = channel.basic_consume(queue='rpc_queue', on_message_callback=callback)

print('*** Await RPC requests.')

channel.start_consuming()
