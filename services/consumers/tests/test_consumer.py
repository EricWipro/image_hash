import base64
from decouple import config
import pika
import uuid


BROKER = config('BROKER_URL')
BROKER_PORT = config('BROKER_PORT')
USER = config('USERRABBITMQ')
PASSWORD = config('PASSRABBITMQ')
VHOST = config('VHOST')

credentials = pika.PlainCredentials(USER, PASSWORD)
parameters = pika.ConnectionParameters(BROKER,
       BROKER_PORT,
       VHOST,
       credentials
    )

def test_consumer(data_img):

    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    queue_declared = channel.queue_declare('', exclusive=True)
    callback_queue = queue_declared.method.queue

    channel.exchange_declare(exchange='image_hash', exchange_type='headers')

    def on_response(ch, method, props, body):
        if corr_id == props.correlation_id:
            print(body)
            response = str(body)

    channel.basic_consume(
        queue = callback_queue,
        on_message_callback = on_response, 
        auto_ack = True
    )


    corr_id = str(uuid.uuid4())
    published_message = channel.basic_publish(exchange='', 
                    routing_key='rpc_queue', 
                    properties=pika.BasicProperties(
                        correlation_id = corr_id,
                        delivery_mode=2,
                        reply_to=callback_queue
                    ),
                    body = data_img
                )
    channel.start_consuming()
    return published_message



file = 'C:\\Users\\ER20259240\\workspace\\image_hash\\services\\consumers\\tests\\img\\front.jpg'
with open(file, 'rb') as img_file:
    data_img = base64.b64encode(img_file.read())
    response = test_consumer(img_file)
