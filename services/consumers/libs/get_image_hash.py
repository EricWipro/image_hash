from PIL import Image, ExifTags
import io
import imagehash
from PIL.ImageOps import mirror
import imagehash
import PIL.Image as Image

class GetImageHash:

    def __init__(self, img):
        self.img = img

    def generate_hash(self):
        hashes = []
        mirrored_image = mirror(self.img)
        hashes.append(str(imagehash.phash(self.img)))
        hashes.append(str(imagehash.phash(mirrored_image)))

        hash = ''.join(sorted(hashes))
        return hash
